const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const users = require('./app/users');
const messages = require('./app/messages');
const app = express();
const port = 8000;
const {nanoid} = require('nanoid');
const Message = require('./models/Message');

require('express-ws')(app)


app.use(express.json());
app.use(cors());
app.use('/users', users);
app.use('/messages', messages);


const activeConnections = {};

app.ws('/chat', (ws, req) => {
    const id = nanoid();
    console.log('Client connected! id=', id);
    activeConnections[id] = ws;

    Object.keys(activeConnections).forEach(async connId => {
        const conn = activeConnections[connId];
        try {
            const messages = await Message.find()
                .populate("user", "username")
                .sort({date: -1})
            conn.send(JSON.stringify({
                type: "NEW_CONNECTION",
                messages: messages.slice(0, 30)
            }));
        } catch (e) {
            console.error(e);
        }
    });

    ws.on('message', (msg) => {
        const decodedData = JSON.parse(msg);
        switch (decodedData.type) {
            case 'CREATE_MESSAGE':
                Object.keys(activeConnections).forEach(async connId => {
                    const conn = activeConnections[connId];
                    try {
                        const newMessage = new Message({
                            user: decodedData.user._id,
                            text: decodedData.text,
                        });
                        await newMessage.save();
                        const populated = await Message.findById(newMessage._id).populate("user", "username");
                        conn.send(JSON.stringify({
                            type: 'NEW_MESSAGE',
                            data: populated
                        }));
                    } catch (e) {
                        console.error(e);
                    }
                });
                break;
            default:
                console.log('Unknown message type:', decodedMessage.type);
        }
    });

    ws.on('close', (msg) => {
        console.log('Client disconnected! id=', id);
        delete activeConnections[id];
    });
})



const run = async () => {
    await mongoose.connect('mongodb://localhost/chat');

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });

    process.on('exit', async () => {
        console.log('exiting');
        await mongoose.disconnect();
    });
};

run().catch(e => console.error(e));