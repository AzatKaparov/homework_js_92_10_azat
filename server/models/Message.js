const mongoose = require('mongoose');
const Shema = mongoose.Schema;

const MessageShema = new Shema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true,
    },
    text: {
        type: String,
        required: true,
    },
    date: {
        type: Date,
        default: () => Date.now() + 7*24*60*60*1000,
    },
});

const Message = mongoose.model('Message', MessageShema);
module.exports = Message;