const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require("./models/User");
const Message = require('./models/Message');

const run = async () => {
    await mongoose.connect(config.db.url);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [user1, user2, user3, user4] = await User.create({
        username: 'admin',
        password: 'admin',
        token: nanoid(),
    }, {
        username: 'root',
        password: 'root',
        token: nanoid(),
    }, {
        username: 'user',
        password: 'user',
        token: nanoid(),
    }, {
        username: 'azat',
        password: 'azat',
        token: nanoid(),
    });

    await Message.create({
        user: user1,
        text: "Test",
    }, {
        user: user2,
        text: "Hello world!"
    });

    await mongoose.connection.close();
};

run().catch(console.error);