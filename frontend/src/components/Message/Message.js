import React from 'react';
import './Message.css';

const Message = ({user, text}) => {
    return (
        <div className="Message border">
            <h6>{user}</h6>
            <p>{text}</p>
        </div>
    );
};

export default Message;