import React from 'react';
import NavItem from "../NavItem/NavItem";

const AnonymousMenu = () => {
    return (
        <>
            <NavItem to="/register" title="Register"/>
            <NavItem to="/login" title="Login"/>
        </>
    );
};

export default AnonymousMenu;