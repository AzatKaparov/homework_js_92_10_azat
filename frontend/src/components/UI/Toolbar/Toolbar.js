import React from 'react';
import {Nav} from "react-bootstrap";
import NavItem from "../NavItem/NavItem";
import {useSelector} from "react-redux";
import UserMenu from "../Menus/UserMenu";
import AnonymousMenu from "../Menus/AnonymousMenu";

const Toolbar = () => {
    const user = useSelector(state => state.users.user);
    return (
        <header className="container py-2 bg-primary mb-3">
            <Nav className="d-flex align-items-center">
                <NavItem to="/" title="Chat"/>
                {!!user
                    ? <UserMenu/>
                    : <AnonymousMenu/>
                }
            </Nav>
        </header>
    );
};

export default Toolbar;