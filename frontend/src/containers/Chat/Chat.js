import React, {useEffect, useRef, useState} from 'react';
import "./Chat.css"
import {Button, Form} from "react-bootstrap";
import {useSelector} from "react-redux";
import Message from "../../components/Message/Message";

const Chat = () => {
    const [messages, setMessages] = useState([]);
    const [messageText, setMessageText] = useState("");
    const { user } = useSelector(state => state.users);
    const ws = useRef(null);

    const onInputChange = e => {
        setMessageText(e.target.value)
    }

    const onFormSubmit = e => {
        e.preventDefault();

        ws.current.send(JSON.stringify({
            type: "CREATE_MESSAGE",
            text: messageText,
            user: user
        }))
        setMessageText("");
    }

    useEffect(() => {
        ws.current = new WebSocket('ws://localhost:8000/chat');

        ws.current.onmessage = e => {
            const decodedData = JSON.parse(e.data);
            console.log(decodedData);
            switch (decodedData.type) {
                case "NEW_MESSAGE": {
                    setMessages(messages => [...messages, {
                        user: decodedData.data.user,
                        text: decodedData.data.text,
                        _id: decodedData.data._id
                    }]);
                }
                break;
                case "NEW_CONNECTION":
                    setMessages(decodedData.messages);
                    break;
                default:
                    break
            }
        }

        ws.current.onclose = () => console.log("ws closed");

        return () => ws.current.close();
    }, [])

    return (
        <div className="container d-flex">
            <div className="online-users border">
                <h2>Online users</h2>
            </div>
            <div className="chat-room border d-flex flex-column justify-content-between">
                <h2>Chat room</h2>
                <div className="messages border d-flex flex-column align-items-end py-2">
                    {messages.map(message => (
                        <Message key={message._id} text={message.text} user={message.user.username} />
                    ))}
                </div>
                <div className="send-message">
                    <Form onSubmit={onFormSubmit}>
                        <Form.Group className="d-flex">
                            <Form.Control
                                className="py-4"
                                onChange={onInputChange}
                                name="messageText"
                                value={messageText}
                                required={true}
                                type="text"
                                placeholder="Enter message"
                            />
                            <Button type="submit">Send</Button>
                        </Form.Group>
                    </Form>
                </div>
            </div>
        </div>
    );
};

export default Chat;