import './App.css';
import {Redirect, Route, Switch} from "react-router-dom";
import Layout from "./components/UI/Layout/Layout";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import {useSelector} from "react-redux";
import Chat from "./containers/Chat/Chat";


const App = () => {
    const user = useSelector(state => state.users.user);

    const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
        return isAllowed ?
            <Route {...props}/> :
            <Redirect to={redirectTo}/>
    };

    return (
        <div className="App">
            <Layout>
                <Switch>
                    <ProtectedRoute
                        path="/"
                        component={Chat}
                        isAllowed={!!user}
                        redirectTo="/login"
                        exact
                    />
                    <Route path="/register" exact component={Register}/>
                    <Route path="/login" exact component={Login}/>
                    <Route render={() => <h1>Not found</h1>}/>
                </Switch>
            </Layout>
        </div>
    );
};

export default App;
